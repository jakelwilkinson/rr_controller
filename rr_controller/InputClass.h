#include "preferences.h"

#define NOAXIS 0
#define XAXIS 1
#define YAXIS 2
#define ZAXIS 3
#define XROTATION 4
#define YROTATION 5
#define ZROTATION 6

#define RUDDERAXIS 8
#define THROTTLEAXIS 7 // slider0 on bluetoothHID
#define ACCELERATORAXIS 9
#define BRAKEAXIS 10
#define STEERINGAXIS 11

int OUTPUTMIN = -32767;
int OUTPUTMAX = 32767;

#if defined(ESP32)
#define PWM_MAX 4095
#else
#define PWM_MAX 1023
#endif

// void WriteInt(int idx, int val)
// {
//   EEPROM.write(idx, val);
//   EEPROM.write(idx + 1, val >> 8);
// }

// int ReadInt(int idx)
// {
//   int val;

//   val = (EEPROM.read(idx + 1) << 8);
//   val |= EEPROM.read(idx);

//   return val;
// }

class Input
{
private:
  uint16_t _val = 0;
  uint16_t _rawVal = 0;
  uint16_t _prev;
  uint8_t _pin;
  uint8_t _pinMode = INPUT_PULLUP;
  uint8_t _isAnalog = false;
  uint16_t _minVal = 0;
  uint16_t _centerVal = PWM_MAX / 2;
  uint16_t _maxVal = PWM_MAX;
  uint16_t _deadZone = 8;
  bool _isInverted = false;

  uint8_t _assignedInput = 0;

  long _lastChange = 0; // used for flashing lights

public:
  Input()
  {
  }

  void InitInput()
  {
    //Serial.println(_pin);
    pinMode(_pin, _pinMode);
    //delay(500);
  }

  void SetToDefaults()
  {
    _pinMode = INPUT_PULLUP;
    _isAnalog = false;
    _minVal = 0;
    _centerVal = PWM_MAX / 2;
    _maxVal = PWM_MAX;
    _deadZone = 8;
    _isInverted = false;

    _assignedInput = 0;
  }

  void Set(int property, int value)
  {
    switch (property)
    {
    case 1:
      // dont set pin this way
      break;
    case 2:
      _pinMode = value;
      break;
    case 3:
      _isAnalog = value;

      break;
    case 4:
      _assignedInput = value;
      break;
    case 5:
      _minVal = value;
      break;
    case 6:
      _centerVal = value;
      break;
    case 7:
      _maxVal = value;
      break;
    case 8:
      _deadZone = value;
      break;
    case 9:
      _isInverted = value;
      break;
    case 10:
      InitInput();
      break;
    }
    pinMode(_pin, _pinMode);
  }

  void PrintVal()
  {
    Serial.print(_rawVal);
    Serial.print("\t");
    Serial.print(_val);
  }

  void ReportConfig()
  {

    Serial.write(_pin);

#if defined(ESP32)
    // pinMode constants are different for ESP32, convert them when communicating with software
    switch (_pinMode)
    {
    case 1: // INPUT
      Serial.write(0);
      break;
    case 5: // INPUT_PULLUP
      Serial.write(2);
      break;
    case 2: // OUTPUT
      Serial.write(1);
      break;
    default:
      Serial.write(3);
      break;
    }

#else
    Serial.write(_pinMode);
#endif

    Serial.write(_isAnalog);
    Serial.write(_isInverted);

    CorrectValForSerial(highByte(_minVal));
    CorrectValForSerial(lowByte(_minVal));

    CorrectValForSerial(highByte(_centerVal));
    CorrectValForSerial(lowByte(_centerVal));

    CorrectValForSerial(highByte(_maxVal));
    CorrectValForSerial(lowByte(_maxVal));

    CorrectValForSerial(highByte(_deadZone));
    CorrectValForSerial(lowByte(_deadZone));
    Serial.write(_assignedInput);

    // 13 bytes
  }

  void CorrectValForSerial(uint8_t raw)
  {
    uint8_t val = raw;
    if (val == 10)
    {
      val += 1;
    }
    else if (val == 13)
    {
      val -= 1;
    }
    Serial.write(val);
  }

  void ReportValues()
  {
    //Serial.write(highByte(_rawVal));
    if (highByte(_rawVal) == 10 || highByte(_rawVal) == 13)
    {
      Serial.write(highByte(_rawVal) + 1);
    }
    else
    {
      Serial.write(highByte(_rawVal));
    }
    // test to avoid sending \r\n 10 13 by serial eol by accident
    if (lowByte(_rawVal) == 10 || lowByte(_rawVal) == 13)
    {
      Serial.write(lowByte(_rawVal) + 1);
    }
    else
    {
      Serial.write(lowByte(_rawVal));
    }

    //Serial.write(highByte(_val));
    if (highByte(_val) == 10 || highByte(_val) == 13)
    {
      Serial.write(highByte(_val) + 1);
    }
    else
    {
      Serial.write(highByte(_val));
    }
    if (lowByte(_val) == 10 || lowByte(_val) == 13)
    {
      Serial.write(lowByte(_val) + 1);
    }
    else
    {
      Serial.write(lowByte(_val));
    }
  }

  void ReportConfigReadable()
  {
    Serial.print(_pin);
    Serial.print(" ");
    Serial.print(_pinMode);
    Serial.print(" ");
    Serial.print(_isAnalog);
    Serial.print(" ");
    Serial.print(_isInverted);
    Serial.print("\r\n");
  }

  void WriteToEEPROM(int idx)
  {
    idx = idx * 14;

    Store8BitValue(idx + 0, _pin);
    Store8BitValue(idx + 1, _pinMode);

    Store8BitValue(idx + 2, _isAnalog);
    Store16BitValue(idx + 3, _minVal);
    Store16BitValue(idx + 5, _centerVal);
    Store16BitValue(idx + 7, _maxVal);

    Store16BitValue(idx + 9, _deadZone);
    Store8BitValue(idx + 11, _isInverted);
    Store8BitValue(idx + 12, _assignedInput);
  }

  void ReadFromEEPROM(int idx)
  {
    idx = idx * 14;

    //_pin = Read8BitValue(idx + 0);
    _pinMode = Read8BitValue(idx + 1);
//    Serial.print("pinMode:");
//    Serial.println(_pinMode);
    // Serial.print("\t");
    // Serial.println(INPUT_PULLUP);

    _isAnalog = Read8BitValue(idx + 2);
    _minVal = Read16BitValue(idx + 3);
    _centerVal = Read16BitValue(idx + 5);
    _maxVal = Read16BitValue(idx + 7);

    _deadZone = Read16BitValue(idx + 9);
    _isInverted = Read8BitValue(idx + 11);
    _assignedInput = Read8BitValue(idx + 12);
  }

  int Mid(int v1, int v2)
  {
    return (v1 + v2) / 2;
  }

  // OUTPUT Variable mapping
  // _isAnalog 0=alwaysLOW, 1=alwaysHIGH, 2=flashing, 3=pulsing, 4=onInRange
  // _minVal flashDelay(milliSeconds)

  void UpdateOutput()
  {
    switch (_isAnalog)
    {         // outputType
    case (0): // alwaysLOW
      digitalWrite(_pin, LOW);
      _val = 0;
      break;
    case (1): // alwaysHIGH
      digitalWrite(_pin, HIGH);
      _val = 1;
      break;
    case (2): // flashing

      FlashOutput();
      break;
    case (3): // pulsing
      PulseOutput();
      break;
    case (4): // onInRange
      digitalWrite(_pin, HIGH);
      break;
    }
  }

  void FlashOutput()
  {
    if (millis() - _lastChange > _minVal)
    {
      _val = digitalRead(_pin);
      digitalWrite(_pin, !_val);
      _lastChange = millis();
    }
  }

  void PulseOutput()
  {
    if (millis() - _lastChange > _minVal)
    {
      _val = ((sin(_rawVal * (3.1412 / 180))) + 1) * 127;

#if !defined(ESP32)
      analogWrite(_pin, _val);
#endif

      _rawVal += 5;
      if (_rawVal >= 360)
      {
        _rawVal = 0;
      }
      _lastChange = millis();
    }
  }

  void UpdateInput()
  {
    _prev = _val;

    if (_pinMode == OUTPUT)
    {
      UpdateOutput();
    }
    else if (_isAnalog)
    {
      int _tempval = 0;
      _rawVal = analogRead(_pin);
      _tempval = _rawVal;

      if (_tempval < _centerVal - _deadZone / 2)
      {
        _tempval = map(_tempval, _minVal, _centerVal - _deadZone / 2, OUTPUTMIN, 0);
        if (_tempval > 0)
        {
          _tempval = OUTPUTMIN;
        }
      }
      else if (_tempval >= _centerVal + _deadZone / 2)
      {
        _tempval = map(_tempval, _centerVal + _deadZone / 2, _maxVal, 0, OUTPUTMAX);
        if (_tempval < 0)
        {
          _tempval = OUTPUTMAX;
        }
      }
      else
      {
        _tempval = 0;
      }

      //_tempval = map(_tempval, _minVal, _maxVal, OUTPUTMIN, OUTPUTMAX);

      // if (_tempval < OUTPUTMIN)
      // {
      //   _tempval = OUTPUTMIN;
      // }
      // else if (_tempval > OUTPUTMAX)
      // {
      //   _tempval = OUTPUTMAX;
      // }

      if (_isInverted)
      {
        _tempval = -_tempval;
      }

      // if (_tempval < _centerVal)
      // {
      //   _tempval = map(_tempval, _minVal, _centerVal, OUTPUTMIN, Mid(OUTPUTMIN, OUTPUTMAX));
      // }
      // else
      // {

      //   _tempval = map(_tempval, _centerVal, _maxVal, Mid(OUTPUTMIN, OUTPUTMAX), OUTPUTMAX);
      // }

      // if (inDeadZone(_tempval))
      // {
      //   _tempval = 511;
      // }
      // else
      // {
      //   if (_tempval < _centerVal)
      //   {
      //     _tempval = map(_tempval, 0, Mid(OUTPUTMIN, OUTPUTMAX) - _deadZone, 0, Mid(OUTPUTMIN, OUTPUTMAX));
      //   }
      //   else
      //   {

      //     _tempval = map(_tempval, Mid(OUTPUTMIN, OUTPUTMAX) + _deadZone, OUTPUTMAX, Mid(OUTPUTMIN, OUTPUTMAX), OUTPUTMAX);
      //   }
      // }

      // if (_tempval > 2000)
      // { // if it overflowed below zero
      //   _tempval = 0;
      // }
      // else if (_tempval > OUTPUTMAX)
      // {
      //   _tempval = OUTPUTMAX;
      // }
      // else if (_tempval < OUTPUTMIN)
      // {
      //   _tempval = OUTPUTMIN;
      // }

      // if (_isInverted)
      // {
      //   _tempval = OUTPUTMAX - _tempval;
      // }
      _val = _tempval;
    }
    else
    {
      _rawVal = !digitalRead(_pin);
      _val = _rawVal;

      if (_isInverted)
      {
        _val = !_val;
      }
    }
  }

  int GetVal()
  {
    if (IsInput())
    {
      return _val;
    }
    else
    {
      return 0;
    }
  }

  void SetVal(int v)
  {
    _val = v;
  }

  void IncrementVal()
  {
    if (!_isInverted)
    {
      _val += _centerVal;

      if (_val > _maxVal)
        _val = _maxVal;
    }
    else
    {
      _val -= _centerVal;

      if (_val < _minVal || _val > _maxVal)
      {
        _val = _minVal;
      }
    }
  }

  void DecrementVal()
  {
    if (!_isInverted)
    {
      _val -= _centerVal;

      if (_val < _minVal || _val > _maxVal)
      {
        _val = _minVal;
      }
    }
    else
    {
      _val += _centerVal;

      if (_val > _maxVal)
        _val = _maxVal;
    }
  }

  bool IsInput()
  {
    return _pinMode != OUTPUT;
  }

  int GetRawVal()
  {
    if (IsInput())
    {
      return _rawVal;
    }
    else
    {
      return 0;
    }
  }

  void SetRawVal(int v)
  {
    _rawVal = v;
  }

  int GetPrev()
  {
    return _prev;
  }

  void SetAssignedInput(int assignedInput)
  {
    _assignedInput = assignedInput;
  }

  uint8_t GetAssignedInput()
  {
    return _assignedInput;
  }

  void SetPin(int pin)
  {
    _pin = pin;
  }

  uint8_t GetPin()
  {
    return _pin;
  }

  void SetPinMode(int pMode)
  {
    _pinMode = pMode;
  }

  uint8_t GetPinMode()
  {
    return _pinMode;
  }

  void SetIsAnalog(uint8_t isAnalog)
  {
    _isAnalog = isAnalog;
  }

  bool IsAnalog()
  {
    return _isAnalog;
  }

  void SetMinVal(int minVal)
  {
    _minVal = minVal;
  }

  uint16_t GetMinVal()
  {
    return _minVal;
  }

  void SetCenterVal(int centerVal)
  {
    _centerVal = centerVal;
  }

  uint16_t GetCenterVal()
  {
    return _centerVal;
  }

  void SetMaxVal(int maxVal)
  {
    _maxVal = maxVal;
  }

  uint16_t GetMaxVal()
  {
    return _maxVal;
  }

  void SetDeadZone(int deadZone)
  {
    _deadZone = deadZone;
  }

  bool inDeadZone(int val)
  {
    if (val > 511 - _deadZone && val < 511 + _deadZone)
    {
      return true;
    }
    return false;
  }

  uint16_t GetDeadZone()
  {
    return _deadZone;
  }

  void SetIsInverted(bool isInverted)
  {
    _isInverted = isInverted;
  }

  bool GetIsInverted()
  {
    return _isInverted;
  }
};
