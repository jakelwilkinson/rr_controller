#define SER_SEPARATOR '\t'
#define SER_EOL "\r\n"
#define SER_INCOMING_HANDSHAKE 74
#define SER_HANDSHAKE_RESPONSE 73
#define SER_REQUEST_ID 76
#define SER_HEADER_ID_RESPONSE 77
#define SER_HEADER_REQUEST_GPIO_CONFIG 80
#define SER_HEADER_REQUEST_GPIO_CONFIG_RESPONSE 81
#define SER_HEADER_GPIO_CONFIG_UPDATE 82
#define SER_HEADER_GPIO_CONFIG_UPDATE_RESPONSE 83
#define SER_HEADER_ENCODER_CONFIG_UPDATE 94
#define SER_HEADER_ENCODER_CONFIG_UPDATE_RESPONSE 95
#define SER_HEADER_MATRIX_CONFIG_UPDATE 92
#define SER_HEADER_MATRIX_CONFIG_UPDATE_RESPONSE 93
#define SER_HEADER_INPUT_VALUES_TRANSMIT 84
#define SER_HEADER_REQUEST_COMMIT_TO_EEPROM 85
#define SER_SEND_TEXT_MESSAGE 86
#define SER_HEADER_SELECT_SUB_DEVICE 87
#define SER_HEADER_UPDATE_DEVICE_NAME 89
#define SER_HEADER_UPDATE_DEVICE_ADDRESS 91
#define SER_HEADER_RESET_TO_DEFAULTS 72

#define PACKET_LENGTH 32
#define COMMAND_SIZE 292

// Device Types
#define UNKNOWN 0
#define ATmega328p 1
#define ATmega32U4 2
#define ATmega2560 3
#define _ESP32 4

#define SERIAL_REPORTING true

static char data[COMMAND_SIZE];
static int serial_count;

static int c;

bool connectedToPC = false;

int configuratorSubdeviceSelection = 0;

void InitComms()
{
    Serial.begin(115200);
    delay(400);
}

void CheckComms()
{
    while (Serial.available() > 0)
    {
        c = Serial.read();
        if (serial_count < COMMAND_SIZE)
        {
            data[serial_count] = c;
            serial_count++;
        }
    }

    // Incoming packet should be at least one byte plus the EOL \r\n
    if (serial_count > 2)
    {
        //Serial.println(serial_count);
        if (data[serial_count - 2] == '\r' && data[serial_count - 1] == '\n')
        {
            InterpretData();
        }
        ClearData();
    }
    else
    {
        ClearData();
    }
}

void InterpretData()
{
  delay(20);
//     Serial.println("interpreting");
//     for (int i = 0; i < serial_count; i++){
//         Serial.print(data[i]);
//     }

    if (data[0] == '\r' || data[0] == '\n')
    {
        return;
    }

    // HANDSHAKE REQUEST FROM PC
    if (data[0] == SER_INCOMING_HANDSHAKE)
    {
        //Serial.println("Got Handshake Request");
        Serial.write(SER_HANDSHAKE_RESPONSE);
        Serial.write(false);
        Serial.write(SER_EOL);
        connectedToPC = true;
    }
    else if (data[0] == '?')
    {
        Serial.println(SubDeviceCount());
    }
    else if (data[0] == SER_REQUEST_ID)
    {
        Serial.write(SER_HEADER_ID_RESPONSE); // 0
        Serial.write(deviceName);             // 16
        Serial.write(firmwareVersion);        // 17
        Serial.write(GetDeviceType());        // 18
        Serial.write(i2c_address);            // 19
        Serial.write(SubDeviceCount());       // 20
        for (int i = 0; i < SubDeviceCount(); i++)
        {
            PrintSubdeviceDeviceID(i);
        }
        Serial.write(SER_EOL);
    }
    else if (data[0] == SER_HEADER_SELECT_SUB_DEVICE)
    {
        if (data[1] <= SubDeviceCount())
        {
            configuratorSubdeviceSelection = data[1] + 1;
        }
        Serial.write(SER_SEND_TEXT_MESSAGE);
        Serial.write("selected subdevice:");
        Serial.write((data[1] + 1));
        Serial.write(SER_EOL);
    }
    else if (data[0] == SER_HEADER_REQUEST_GPIO_CONFIG)
    {
        Serial.write(SER_HEADER_REQUEST_GPIO_CONFIG_RESPONSE);
        configuratorSubdeviceSelection = data[1];
        ReportInputConfig(data[1]); // data[1] should be the requested subDevice address index

        Serial.write(SER_EOL);
    }
    else if (data[0] == SER_HEADER_UPDATE_DEVICE_NAME)
    {
        if (configuratorSubdeviceSelection == 0)
        {
            if (serial_count < 19)
            {
                Serial.write(SER_SEND_TEXT_MESSAGE);
                Serial.write("not enough bytes for deviceName:");
                Serial.write(SER_EOL);
                return;
            }
            for (int i = 0; i < 16; i++)
            {
                deviceName[i] = data[i + 1];
            }
        }
        else
        {
            SendUpdateDeviceName(data, configuratorSubdeviceSelection - 1);
        }
    }
    else if (data[0] == SER_HEADER_UPDATE_DEVICE_ADDRESS)
    {
        if (configuratorSubdeviceSelection == 0)
        {
            i2c_address = data[1];
        }
        else
        {
            SendUpdateDeviceAddress(data[1], configuratorSubdeviceSelection - 1);
        }
    }
    else if (data[0] == SER_HEADER_REQUEST_COMMIT_TO_EEPROM)
    {
        if (configuratorSubdeviceSelection == 0)
        {
            SaveCurrentInputConfig();
        }
        else
        {
            OrderSaveToEEPROM(configuratorSubdeviceSelection - 1);
        }
        //CommitButtonMatrixToEEPROM();
        Serial.write(SER_SEND_TEXT_MESSAGE);
        Serial.print("Config saved to EEPROM");
        Serial.write(SER_EOL);
    }
    else if (data[0] == SER_HEADER_MATRIX_CONFIG_UPDATE)
    {
        Serial.write(SER_HEADER_MATRIX_CONFIG_UPDATE_RESPONSE);
        Serial.write(configuratorSubdeviceSelection);

        if (configuratorSubdeviceSelection == 0)
        {
            for (int i = 0; i < 16; i++)
            {
                SetMatrixRowPin(i, data[1 + i]);
                SetMatrixColPin(i, data[1 + 16 + i]);
            }

            for (int i = 0; i < 256; i++)
            {
                SetMatrixInputAssignment(i, data[1 + 32 + i]);
            }
        }
        else
        {
            SendMatrixConfigUpdate(data, configuratorSubdeviceSelection - 1);
        }

        Serial.write(SER_EOL);
    }
    else if (data[0] == SER_HEADER_GPIO_CONFIG_UPDATE && serial_count == 18)
    {
        Serial.write(SER_HEADER_GPIO_CONFIG_UPDATE_RESPONSE);
        Serial.write(configuratorSubdeviceSelection);
        if (configuratorSubdeviceSelection == 0)
        { // address == 0
        
#if defined(ESP32)
            // pinMode constants are different for ESP32, convert them when communicating with software
            Serial.write(SER_SEND_TEXT_MESSAGE);
            switch (data[4])
            {
            case 0:                          // INPUT
                SetInput(data[2], 2, INPUT); // pinMode
                Serial.write(data[4]);
                //Serial.print(" converted to ");
                Serial.write(INPUT);
                break;
            case 1:                           // OUTPUT
                SetInput(data[2], 2, OUTPUT); // pinMode
                Serial.write(data[4]);
                // Serial.print(" converted to ");
                Serial.write(OUTPUT);
                break;
            case 2:                                 // INPUT_PULLUP
                SetInput(data[2], 2, INPUT_PULLUP); // pinMode
                Serial.write(data[4]);
                //Serial.print(" converted to ");
                Serial.write(INPUT_PULLUP);
                break;
            }
            //Serial.print(SER_EOL);

#else
            SetInput(data[2], 2, data[4]); // pinMode
#endif

            SetInput(data[2], 3, data[5]); // isAnalog
            Serial.write(data[5]);
            SetInput(data[2], 9, data[6]); // isInverted

            uint8_t hb = data[7];
            uint8_t lb = data[8];
            uint16_t v = (hb << 8) + lb;
            SetInput(data[2], 5, v); // minval

            hb = data[9];
            lb = data[10];
            v = (hb << 8) + lb;
            SetInput(data[2], 6, v); // midval

            hb = data[11];
            lb = data[12];
            v = (hb << 8) + lb;
            SetInput(data[2], 7, v); // maxval

            hb = data[13];
            lb = data[14];
            v = (hb << 8) + lb;
            SetInput(data[2], 8, v); // deadzone

            SetInput(data[2], 4, data[15]); // assignedInput
        }
        else
        {
            SendConfigUpdate(data, configuratorSubdeviceSelection - 1);
        }
        Serial.write(SER_EOL);
    }
    else if (data[0] == SER_HEADER_ENCODER_CONFIG_UPDATE && serial_count == 14)
    {
        Serial.write(SER_HEADER_ENCODER_CONFIG_UPDATE_RESPONSE);
        Serial.write(configuratorSubdeviceSelection);
        if (configuratorSubdeviceSelection == 0){
            SetEncoder(data[1], 1, data[2]); // pin a idx
            SetEncoder(data[1], 2, data[3]); // pin b idx
            SetEncoder(data[1], 5, data[4]); // left assignment
            SetEncoder(data[1], 6, data[5]); // right assignment
        } else {
            SendEncoderConfigUpdate(data, configuratorSubdeviceSelection - 1);
        }
        //GetEncoder(data[1]).ReportConfig();

        Serial.write(SER_EOL);

    }
    else if (data[0] == SER_HEADER_RESET_TO_DEFAULTS)
    {
        
        if (configuratorSubdeviceSelection == 0)
        {
            ResetToDefaults();
        }
        else
        {
            ResetSubDeviceToDefaults(configuratorSubdeviceSelection - 1);
        }

        Serial.write(SER_HEADER_RESET_TO_DEFAULTS);
        Serial.write(configuratorSubdeviceSelection);
        Serial.write(SER_EOL);
    }
    else
    {
        Serial.write(SER_SEND_TEXT_MESSAGE);
        for (int i = 0; i < serial_count - 2; i++)
        {
            Serial.write(data[i]);
        }
        Serial.write(SER_EOL);
    }
}

void SentTextMessage(char *msg)
{
    Serial.write(SER_SEND_TEXT_MESSAGE);
    Serial.write(msg);
    Serial.write(SER_EOL);
}

void SendInputValuesToSerial()
{
    if (!SERIAL_REPORTING)
    {
        return;
    }

    Serial.write(SER_HEADER_INPUT_VALUES_TRANSMIT);
    if (configuratorSubdeviceSelection == 0)
    {
        for (int i = 0; i < InputCount(); i++)
        {
            GetInput(i).ReportValues();
        }
        ReportMatrixState();
    }
    else
    {
        PrintSubdeviceInputValuesFull(configuratorSubdeviceSelection - 1);

        PrintSubDeviceMatrixState(configuratorSubdeviceSelection - 1);
    }

    Serial.write(SER_EOL);
}

void ClearData()
{
    for (int i = 0; i < COMMAND_SIZE; i++)
    {
        data[i] = 0;
    }
    serial_count = 0;
}

bool IsConnectedToPC()
{
    return connectedToPC;
}

uint8_t GetDeviceType()
{
    uint8_t dType = UNKNOWN;

#if defined(__AVR_ATmega2560__)
    dType = ATmega2560;
#elif defined(__AVR_ATmega328P__)
    dType = ATmega328p;
#elif defined(__AVR_ATmega32U4__)
    dType = ATmega32U4;
#elif defined(ESP32)
    dType = _ESP32;
#else
    dType = UNKNOWN;
#endif

    return dType;
}
